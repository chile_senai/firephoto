package br.senai.sp.informatica.firephoto.service;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

import br.senai.sp.informatica.firephoto.activity.LoginActivity;
import br.senai.sp.informatica.firephoto.util.SharedPrefsUtil;

/**
 * Created by SENAI on 20/03/2017.
 */

public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    // método executado quando o aplicativo recebe ou atualiza um foken FCM
    @Override
    public void onTokenRefresh() {
        // obtém o token atualizado
        String fcmToken = FirebaseInstanceId.getInstance().getToken();
        // salva o token nas shareds prefs
        SharedPrefsUtil.saveFcmToken(getApplicationContext(), fcmToken);
    }
}
