package br.senai.sp.informatica.firephoto.modelo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by SENAI on 23/03/2017.
 */

public class Foto {
    private String id;
    private String titulo;
    private String localArmazenamento;
    private String url;
    private String fcmToken;
    private String emailUser;
    private List<String> likes = new ArrayList<>();

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getFcmToken() {
        return fcmToken;
    }

    public void setFcmToken(String fcmToken) {
        this.fcmToken = fcmToken;
    }

    public String getEmailUser() {
        return emailUser;
    }

    public void setEmailUser(String emailUser) {
        this.emailUser = emailUser;
    }

    public String getLocalArmazenamento() {
        return localArmazenamento;
    }

    public void setLocalArmazenamento(String localArmazenamento) {
        this.localArmazenamento = localArmazenamento;
    }

    public List<String> getLikes() {
        return likes;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        Foto foto = (Foto) obj;
        if (foto.getId().equals(this.getId())) {
            return true;
        } else {
            return false;
        }
    }
}
