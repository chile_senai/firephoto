package br.senai.sp.informatica.firephoto.adapter;

import android.content.Context;
import android.support.v7.widget.ButtonBarLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.List;

import br.senai.sp.informatica.firephoto.R;
import br.senai.sp.informatica.firephoto.activity.TelaInicialActivity;
import br.senai.sp.informatica.firephoto.modelo.Foto;

/**
 * Created by SENAI on 24/03/2017.
 */

public class FotoAdapter extends RecyclerView.Adapter<FotoAdapter.FotoViewHolder> {
    private List<Foto> fotos;
    private Context context;
    private FotoListener fotoListener;

    public FotoAdapter(List<Foto> fotos, Context context, FotoListener fotoListener) {
        this.fotos = fotos;
        this.context = context;
        this.fotoListener = fotoListener;
    }

    @Override
    public FotoViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.adapter_foto, parent, false);
        return new FotoViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final FotoViewHolder holder, int position) {
        final Foto foto = fotos.get(position);
        holder.textTitulo.setText(foto.getTitulo());
        holder.textEmail.setText(foto.getEmailUser());
        holder.textLikes.setText(context.getString(R.string.likes, foto.getLikes().size()));

        if (foto.getEmailUser().equals(TelaInicialActivity.usuario.getEmail())) {
            holder.btExcluir.setVisibility(View.VISIBLE);
            holder.btExcluir.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fotoListener.onClickExcluir(foto);
                }
            });
        } else {
            holder.btExcluir.setVisibility(View.GONE);
        }

        if (foto.getEmailUser().equals(TelaInicialActivity.usuario.getEmail())) {
            holder.btLike.setVisibility(View.GONE);
        } else if (foto.getLikes().contains(TelaInicialActivity.usuario.getUid())) {
            holder.btLike.setImageResource(R.drawable.ic_like_press);
            holder.btLike.setOnClickListener(null);
        } else {
            holder.btLike.setImageResource(R.drawable.ic_like);
            holder.btLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    holder.btLike.setImageResource(R.drawable.ic_like_press);
                    fotoListener.onClickLike(foto);
                }
            });
        }

        Picasso.with(context).load(foto.getUrl()).into(holder.imageFoto, new Callback() {
            @Override
            public void onSuccess() {
                holder.progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                holder.progressBar.setVisibility(View.GONE);
                holder.imageFoto.setImageResource(android.R.drawable.ic_menu_close_clear_cancel);
            }
        });
    }

    @Override
    public int getItemCount() {
        return fotos.size();
    }

    class FotoViewHolder extends RecyclerView.ViewHolder {
        ImageView imageFoto;
        TextView textTitulo, textEmail, textLikes;
        ProgressBar progressBar;
        ImageButton btExcluir, btLike;

        public FotoViewHolder(View itemView) {
            super(itemView);

            imageFoto = (ImageView) itemView.findViewById(R.id.image_foto);

            textTitulo = (TextView) itemView.findViewById(R.id.text_titulo);

            textEmail = (TextView) itemView.findViewById(R.id.text_email);

            progressBar = (ProgressBar) itemView.findViewById(R.id.progress_bar);

            btExcluir = (ImageButton) itemView.findViewById(R.id.bt_excluir);

            btLike = (ImageButton) itemView.findViewById(R.id.bt_curtir);

            textLikes = (TextView) itemView.findViewById(R.id.text_likes);
        }
    }

    public interface FotoListener {
        void onClickExcluir(Foto foto);

        void onClickLike(Foto foto);
    }
}
