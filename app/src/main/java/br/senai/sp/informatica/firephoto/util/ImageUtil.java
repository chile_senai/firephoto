package br.senai.sp.informatica.firephoto.util;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.util.Log;

import java.io.IOException;

/**
 * Created by SENAI on 22/03/2017.
 */

public class ImageUtil {
    public static Bitmap corrigeImagem(Bitmap bitmap, String path) {
        Bitmap imagem = null;
        try {
            ExifInterface ei = new ExifInterface(path);
            int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_ROTATE_90:
                    imagem = rotateAndResizeImage(bitmap, 90);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_180:
                    imagem = rotateAndResizeImage(bitmap, 180);
                    break;

                case ExifInterface.ORIENTATION_ROTATE_270:
                    imagem = rotateAndResizeImage(bitmap, 270);
                    break;
                default:
                    imagem = rotateAndResizeImage(bitmap, 0);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return imagem;
    }

    private static Bitmap rotateAndResizeImage(Bitmap source, float angle) {

        Bitmap bitmap = null;
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        try {
            bitmap = Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                    matrix, true);
        } catch (OutOfMemoryError err) {
            err.printStackTrace();
        }
        return bitmap;
    }

}
