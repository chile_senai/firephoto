package br.senai.sp.informatica.firephoto.tasks;

public abstract class HandlerTaskAdapter implements HandlerTask {
    @Override
    public void onPreHandle() {}

    @Override
    public void onSuccess(String valueRead) { }

    @Override
    public void onError(Exception erro) {}
}