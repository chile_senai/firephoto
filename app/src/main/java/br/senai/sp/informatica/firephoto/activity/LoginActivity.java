package br.senai.sp.informatica.firephoto.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

import br.senai.sp.informatica.firephoto.R;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    private EditText editEmail;
    private EditText editSenha;
    private ProgressBar progressBar;
    private ScrollView formLogin;

    // objeto para se logar no firebase
    private FirebaseAuth firebaseAuth;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // coloca a toolbar como actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.bem_vindo);
        setSupportActionBar(toolbar);

        // editEmail
        editEmail = (EditText) findViewById(R.id.edit_email);

        // editSenha
        editSenha = (EditText) findViewById(R.id.edit_senha);

        // progressBar
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        // formLogin
        formLogin = (ScrollView) findViewById(R.id.form_login);

        // o método estático getInstance nos da uma instância de firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();

        // verifica se existe usuário logado, caso exista, inicia a tela inicial
        if (firebaseAuth.getCurrentUser() != null) {
            Intent intent = new Intent(LoginActivity.this, TelaInicialActivity.class);
            startActivity(intent);
            finish();
            return;
        }

        // adiciona o listener à instância do firebaseAuth
        firebaseAuth.addAuthStateListener(listenerState);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (!connectionResult.isSuccess()) {
            Toast.makeText(this, connectionResult.getErrorMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    // método de login do usuário
    public void acaoClick(View v) {
        // recupera as strings dos dois componentes de tela
        String email = editEmail.getText().toString();
        String senha = editSenha.getText().toString();
        // verifica se o e-mail está vazio
        if (TextUtils.isEmpty(email)) {
            editEmail.setError(getString(R.string.campo_obrigatorio));
            editEmail.requestFocus();
            // verifica se o e-mail é válido (se tem @)
        } else if (!email.contains("@")) {
            editEmail.setError(getString(R.string.valida_email));
            editEmail.requestFocus();
            // verifica se a senha está vazia
        } else if (TextUtils.isEmpty(senha)) {
            editSenha.setError(getString(R.string.campo_obrigatorio));
            editSenha.requestFocus();
            // verifica se a senha está vazia
        } else if (senha.length() < 6) {
            editSenha.setError(getString(R.string.valida_senha));
            editSenha.requestFocus();
        } else {
            // liga a progressbar e oculta o form
            progressBar.setVisibility(View.VISIBLE);
            formLogin.setVisibility(View.GONE);
            // verifica qual View disparou a ação
            switch (v.getId()) {
                // se quem disparaoou a ação foi o botão de logar, realiza o login.
                case R.id.bt_login:
                    // invoca o método do firebaseauth responsável por realizar o login do usuaário com email e senha,
                    // acrescentando um listener que será executado ao ser completada a task
                    firebaseAuth.signInWithEmailAndPassword(email, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // oculta a progressbar
                            progressBar.setVisibility(View.GONE);
                            if (task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, firebaseAuth.getCurrentUser().getEmail() + " logou", Toast.LENGTH_SHORT).show();
                                // inicia a Activity Principal e passa o Usuario na intent
                                Intent intent = new Intent(LoginActivity.this, TelaInicialActivity.class);
                                startActivity(intent);
                                // fecha a activity atual
                                finish();
                            } else {
                                // exibe novamente o formLogin
                                formLogin.setVisibility(View.VISIBLE);
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    break;
                // se quem disparaou a ação foi o text de cadastrar, realiza o cadastro.
                case R.id.txt_cadastro:
                    // invoca o método do firebaseauth responsável por realizar o cadastro do usuário com email e senha,
                    // acrescentando um listener que será executado ao ser completada a task
                    firebaseAuth.createUserWithEmailAndPassword(email, senha).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            // oculta a progressbar
                            progressBar.setVisibility(View.GONE);
                            // exibe novamente o formLogin
                            formLogin.setVisibility(View.VISIBLE);
                            if (task.isSuccessful()) {
                                Toast.makeText(LoginActivity.this, firebaseAuth.getCurrentUser().getEmail() + " cadastrado com sucesso", Toast.LENGTH_SHORT).show();
                            } else {
                                Toast.makeText(LoginActivity.this, task.getException().getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                    break;
            }
        }
    }


    // objeto para controlar as alterações de estado do login
    private FirebaseAuth.AuthStateListener listenerState = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            Log.w("LISTENER", firebaseAuth.getCurrentUser() + "");
        }
    };
}
