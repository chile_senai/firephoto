package br.senai.sp.informatica.firephoto.util;

import android.util.Log;

import com.google.gson.Gson;

import java.util.Arrays;
import java.util.List;


/**
 * Created by José Roberto on 11/03/2017.
 */

public class JsonParser<T> {
    final Class<T> tipoClasse;
    Gson gson = new Gson();

    public JsonParser(Class<T> tipoClasse) {
        this.tipoClasse = tipoClasse;
    }

    public T toObject(String json) {
        return gson.fromJson(json, tipoClasse);
    }

    // pelo fato de T ser genérico, o Type não pode ser recuperado, sendo necessária a referência de vetor da classe
    public List<T> toList(String json, Class<T[]> classe) {
        return Arrays.asList(gson.fromJson(json, classe));
    }

    public String fromObject(T object) {
        return gson.toJson(object);
    }


}