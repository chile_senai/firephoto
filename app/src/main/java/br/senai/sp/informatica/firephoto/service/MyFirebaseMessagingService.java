package br.senai.sp.informatica.firephoto.service;

import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import br.senai.sp.informatica.firephoto.activity.TelaInicialActivity;
import br.senai.sp.informatica.firephoto.util.NotificaUtil;

/**
 * Created by joserobertochilesilva on 26/03/17.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // verifica se a mensagem contém notificação
        if (remoteMessage.getNotification() != null) {
            NotificaUtil.sendNotification(this, TelaInicialActivity.class, remoteMessage.getNotification().getTitle(), remoteMessage.getNotification().getBody());
        }
    }
}
