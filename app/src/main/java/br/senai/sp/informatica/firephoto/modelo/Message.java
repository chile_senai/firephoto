package br.senai.sp.informatica.firephoto.modelo;

/**
 * Created by joserobertochilesilva on 26/03/17.
 */

public class Message {
    private String to;
    private Notification notification;

    public Message(String to, String title, String body) {
        this.to = to;
        this.notification = new Notification(title, body);
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public Notification getNotification() {
        return notification;
    }

    public void setNotification(Notification notification) {
        this.notification = notification;
    }

    class Notification {
        private String title;
        private String body;

        public Notification(String title, String body) {
            this.title = title;
            this.body = body;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getBody() {
            return body;
        }

        public void setBody(String body) {
            this.body = body;
        }
    }
}
