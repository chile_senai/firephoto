package br.senai.sp.informatica.firephoto.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.Preference;
import android.preference.PreferenceManager;

/**
 * Created by José Roberto on 16/03/2017.
 */

public class SharedPrefsUtil {

    public static void saveFcmToken(Context context, String fcmToken) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        SharedPreferences.Editor editor = sharedPrefs.edit();
        editor.putString("fcmToken", fcmToken);
        editor.commit();
    }

    public static String getFcmToken(Context context) {
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(context);
        return sharedPrefs.getString("fcmToken", null);
    }
}
