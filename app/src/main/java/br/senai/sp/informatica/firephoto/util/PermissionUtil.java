package br.senai.sp.informatica.firephoto.util;

import android.app.Activity;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;


public class PermissionUtil {
    public static boolean checarPermissao(Activity activity, int requestCode, String... permissoes) {
        List<String> negadas = new ArrayList<>();
        for (String permissao : permissoes) {
            if (ContextCompat.checkSelfPermission(activity, permissao) != PackageManager.PERMISSION_GRANTED) {
                negadas.add(permissao);
            }
        }
        if (negadas.isEmpty()) {
            return true;
        } else {
            String[] permissoesNegadas = new String[negadas.size()];
            negadas.toArray(permissoesNegadas);
            ActivityCompat.requestPermissions(activity, permissoesNegadas, requestCode);
            return false;
        }
    }
}