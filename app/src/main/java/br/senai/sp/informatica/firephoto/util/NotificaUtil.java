package br.senai.sp.informatica.firephoto.util;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

/**
 * Created by SENAI on 27/03/2017.
 */

public class NotificaUtil {
    public static void sendNotification(Context context, Class classe, String titulo, String mensagem) {
        Intent intent = new Intent(context, classe);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_ONE_SHOT);
        // define um som para a notificação
        Uri somNotificacao = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notifyBuilder = new NotificationCompat.Builder(context);
        notifyBuilder.setSmallIcon(android.R.drawable.ic_menu_info_details);
        notifyBuilder.setContentTitle(titulo);
        notifyBuilder.setContentText(mensagem);
        notifyBuilder.setSound(somNotificacao);
        notifyBuilder.setContentIntent(pendingIntent);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        manager.notify(0, notifyBuilder.build());
    }

}
