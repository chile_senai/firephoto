package br.senai.sp.informatica.firephoto.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.BuildConfig;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;

import br.senai.sp.informatica.firephoto.R;
import br.senai.sp.informatica.firephoto.adapter.FotoAdapter;
import br.senai.sp.informatica.firephoto.modelo.Foto;
import br.senai.sp.informatica.firephoto.util.ImageUtil;
import br.senai.sp.informatica.firephoto.util.PermissionUtil;
import br.senai.sp.informatica.firephoto.util.SharedPrefsUtil;

/**
 * Created by SENAI on 22/03/2017.
 */

public class FotoActivity extends AppCompatActivity {
    private ProgressBar progressBar;
    private ScrollView formFoto;
    private ImageView imageFoto;
    private AlertDialog dialogImagem;
    private File fileFoto;
    private final int CAMERA = 0, GALERIA = 1;
    private Bitmap bmpFoto;
    private EditText editTitulo;

    // variável para acessar o Firebase Storage
    private FirebaseStorage firebaseStorage;

    // variável para a referência do storage de fotos
    private StorageReference fotoReference;

    // variável para database
    private FirebaseDatabase database;

    // variável para coleção de fotos
    private DatabaseReference dbFotos;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_foto);

        // imageFoto
        imageFoto = (ImageView) findViewById(R.id.image_foto);

        // RECUPERA O FILE PARA EXIBIR NO IMAGEVIEW
        if (savedInstanceState != null && (fileFoto = (File) savedInstanceState.getSerializable("fileFoto")) != null) {
            exibirFoto(FileProvider.getUriForFile(getBaseContext(), "br.senai.sp.informatica.firephoto.provider", fileFoto));
        }

        // dialogImagem
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(R.string.origem_imagem);
        String[] itens = {getString(R.string.camera), getString(R.string.galeria)};
        builder.setItems(itens, listenerDialog);
        dialogImagem = builder.create();

        // coloca a toolbar como actionbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(R.string.nova_foto);
        setSupportActionBar(toolbar);

        // progressBar
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);

        // formFoto
        formFoto = (ScrollView) findViewById(R.id.form_foto);

        // editTitulo
        editTitulo = (EditText) findViewById(R.id.edit_titulo);

        // firebaseStorage
        firebaseStorage = FirebaseStorage.getInstance();

        // fotoReference
        fotoReference = firebaseStorage.getReferenceFromUrl("gs://firephoto-7b9b3.appspot.com").child("fotos");

        // database
        database = FirebaseDatabase.getInstance();

        // dbFotos
        dbFotos = database.getReference("fotos");
    }

    public void imageClick(View v) {
        dialogImagem.show();
    }

    private DialogInterface.OnClickListener listenerDialog = new DialogInterface.OnClickListener() {
        @Override
        public void onClick(DialogInterface dialog, int which) {
            Intent intent;
            if (PermissionUtil.checarPermissao(FotoActivity.this, 1, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)) {
                switch (which) {
                    case 0:
                        // Diretório para salvar o arquivo da foto
                        File diretorio = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                        // Cria o nome completo do arquivo que será salvo
                        String nomeFoto = diretorio.getPath() + "/" + System.currentTimeMillis() + ".jpg";
                        fileFoto = new File(nomeFoto);
                        // Chama a intent informando o arquivo para salvar a foto
                        intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, FileProvider.getUriForFile(getBaseContext(), "br.senai.sp.informatica.firephoto.provider", fileFoto));
                        startActivityForResult(intent, CAMERA);
                        break;
                    case 1:
                        intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, GALERIA);
                        break;
                }
            }
        }
    };

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case GALERIA:
                    Uri uri = data.getData();
                    // Utilizar isto para mostrar como vem a consulta através do cursor
                    Cursor cursor = getContentResolver().query(uri, null, null, null, null);
                    cursor.moveToNext();
                    String[] dataColumn = {MediaStore.Images.Media.DATA};
                    int columnIndex = cursor.getColumnIndex(dataColumn[0]);
                    String caminhoArquivo = cursor.getString(columnIndex);
                    cursor.close();
                    fileFoto = new File(caminhoArquivo);
                    exibirFoto(FileProvider.getUriForFile(getBaseContext(), "br.senai.sp.informatica.firephoto.provider", fileFoto));
                    break;
                case CAMERA:
                    exibirFoto(FileProvider.getUriForFile(getBaseContext(), "br.senai.sp.informatica.firephoto.provider", fileFoto));
                    break;
            }
        }
    }

    private void exibirFoto(Uri uriFoto) {
        try {
            bmpFoto = ImageUtil.corrigeImagem(MediaStore.Images.Media.getBitmap(getContentResolver(), uriFoto), uriFoto.getPath());
            imageFoto.setImageBitmap(bmpFoto);
        } catch (IOException erro) {
            Toast.makeText(this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        for (int resultado : grantResults) {
            if (resultado == PackageManager.PERMISSION_DENIED) {
                Toast.makeText(this, R.string.permissao_negada, Toast.LENGTH_SHORT).show();
            } else {
                listenerDialog.onClick(dialogImagem, 1);
            }
        }
    }

    public void salvarClick(View v) {
        String titulo = editTitulo.getText().toString();
        // verifica se o título foi digitado
        if (TextUtils.isEmpty(titulo)) {
            editTitulo.setError(getString(R.string.campo_obrigatorio));
            editTitulo.requestFocus();
            // verifica se existe foto
        } else if (bmpFoto == null) {
            Toast.makeText(this, R.string.foto_obrigatoria, Toast.LENGTH_SHORT).show();
        } else {
            progressBar.setVisibility(View.VISIBLE);
            formFoto.setVisibility(View.GONE);
            final Foto foto = new Foto();
            foto.setEmailUser(TelaInicialActivity.usuario.getEmail());
            foto.setFcmToken(SharedPrefsUtil.getFcmToken(this));
            foto.setTitulo(titulo);
            fotoReference = fotoReference.child(foto.getTitulo().replace(" ", "") + "-" + fileFoto.getName());
            // converte o bitmap em vetor de bytes
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            bmpFoto.compress(Bitmap.CompressFormat.JPEG, 70, baos);
            byte[] bytes = baos.toByteArray();
            // faz o upload dos bytes que representam a imagem
            fotoReference.putBytes(bytes).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    foto.setUrl(taskSnapshot.getDownloadUrl().toString());
                    foto.setLocalArmazenamento(taskSnapshot.getStorage().getPath());
                    dbFotos.push().setValue(foto);
                    finish();
                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Toast.makeText(FotoActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    progressBar.setVisibility(View.GONE);
                    formFoto.setVisibility(View.VISIBLE);
                }
            });
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        if (fileFoto != null)
            outState.putSerializable("fileFoto", fileFoto);
    }


}
