package br.senai.sp.informatica.firephoto.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

import br.senai.sp.informatica.firephoto.BuildConfig;
import br.senai.sp.informatica.firephoto.R;
import br.senai.sp.informatica.firephoto.adapter.FotoAdapter;
import br.senai.sp.informatica.firephoto.modelo.Foto;
import br.senai.sp.informatica.firephoto.modelo.Message;
import br.senai.sp.informatica.firephoto.tasks.HandlerTask;
import br.senai.sp.informatica.firephoto.tasks.HandlerTaskAdapter;
import br.senai.sp.informatica.firephoto.tasks.TaskRest;
import br.senai.sp.informatica.firephoto.util.JsonParser;
import br.senai.sp.informatica.firephoto.util.SharedPrefsUtil;

/**
 * Created by SENAI on 22/03/2017.
 */

public class TelaInicialActivity extends AppCompatActivity implements FotoAdapter.FotoListener {
    private RecyclerView recyclerView;
    private Toolbar toolbar;

    // objeto para se logar no firebase
    private FirebaseAuth firebaseAuth;
    // usuário logado
    public static FirebaseUser usuario;
    // objeto para se conectar à database do firebase
    private FirebaseDatabase database;
    // objeto para obter referência da coleção de fotos
    private DatabaseReference reference;
    // lista de fotos
    private List<Foto> fotos;
    // objeto para FirebaseStorage, referência para as afotos
    private StorageReference storageFoto;
    // objeto para FirebaseRemoteConfig
    private FirebaseRemoteConfig remoteConfig;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inicial);

        // coloca a toolbar como actionbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // recyclerView
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // obtém instância da database
        database = FirebaseDatabase.getInstance();

        // o método estático getInstance nos da uma instância de firebaseAuth
        firebaseAuth = FirebaseAuth.getInstance();
        usuario = firebaseAuth.getCurrentUser();

        // obtém uma instância de Firebasedatabase
        database = FirebaseDatabase.getInstance();

        // instancia a lista
        fotos = new ArrayList<>();
        recyclerView.setAdapter(new FotoAdapter(fotos, this, this));

        // obtém a referência para a coleção de fotos
        reference = database.getReference("fotos");
        reference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                fotos.clear();
                for (DataSnapshot fotoSnapshot : dataSnapshot.getChildren()) {
                    Foto foto = fotoSnapshot.getValue(Foto.class);
                    foto.setId(fotoSnapshot.getKey());
                    fotos.add(foto);
                }
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        reference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                Foto foto = new Foto();
                foto.setId(dataSnapshot.getKey());
                fotos.remove(foto);
                recyclerView.getAdapter().notifyDataSetChanged();
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        // fotoReference
        storageFoto = FirebaseStorage.getInstance().getReferenceFromUrl("gs://firephoto-7b9b3.appspot.com");

        // obtém uma instância de FirebaseRemoteConfig
        remoteConfig = FirebaseRemoteConfig.getInstance();
        // adiciona os valores default
        remoteConfig.setDefaults(R.xml.remote_config_default);
        // habilita o modo Desenvolvedor
        FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder().setDeveloperModeEnabled(true).build();
        remoteConfig.setConfigSettings(configSettings);
        // busca dados no RemoteConfig
        fetchTitulo();

    }

    public void fabClick(View v) {
        startActivity(new Intent(this, FotoActivity.class));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_inicial, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_logout:
                firebaseAuth.signOut();
                finish();
                startActivity(new Intent(this, LoginActivity.class));
        }
        return false;
    }

    @Override
    public void onClickExcluir(final Foto foto) {
        final DatabaseReference referenceFoto = reference.child(foto.getId());
        referenceFoto.removeValue().addOnCompleteListener(new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                StorageReference refFoto = storageFoto.child(foto.getLocalArmazenamento());
                refFoto.delete();
            }
        });
    }

    @Override
    public void onClickLike(Foto foto) {
        foto.getLikes().add(usuario.getUid());
        reference.child(foto.getId()).setValue(foto);
        Message message = new Message(foto.getFcmToken(), "A foto " + foto.getTitulo() + " teve uma curtida!!!", usuario.getEmail() + " curtiu sua foto.");
        TaskRest taskRest = new TaskRest(TaskRest.RequestMethod.POST, handlerAdapter, "key=AAAAJ2dzHVU:APA91bGwFWRQg0Ltk59o2Pvq-smDOUFNMsLw0jBPCM-cliXXjMzI_QpW8l-M3e2V3ARRFzq9P4EZCgtzpT6_bUQacMktPsz6QKwRZc6KWgTHGZkY4qARPWE4yWUav86s3j8X9u2Q-jt0");
        taskRest.execute("https://fcm.googleapis.com/fcm/send", new JsonParser<Message>(Message.class).fromObject(message));
    }

    private HandlerTask handlerAdapter = new HandlerTaskAdapter() {
        @Override
        public void onError(Exception erro) {
            Toast.makeText(TelaInicialActivity.this, erro.getMessage(), Toast.LENGTH_SHORT).show();
        }
    };

    /**
     * Fetch a welcome message from the Remote Config service, and then activate it.
     */
    private void fetchTitulo() {
        toolbar.setTitle(remoteConfig.getString("welcome_message"));
        toolbar.setTitleTextColor(Color.parseColor(remoteConfig.getString("color_text")));

        long cacheExpiration = 3600; // 1 hour in seconds.

        if (remoteConfig.getInfo().getConfigSettings().isDeveloperModeEnabled()) {
            cacheExpiration = 0;
        }

        remoteConfig.fetch(cacheExpiration)
                .addOnCompleteListener(this, new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            remoteConfig.activateFetched();
                        }
                        toolbar.setTitle(remoteConfig.getString("welcome_message"));
                        toolbar.setTitleTextColor(Color.parseColor(remoteConfig.getString("color_text")));
                    }
                });
    }
}
